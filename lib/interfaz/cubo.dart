import 'package:flutter/material.dart';

class Cubo extends StatelessWidget {
  const Cubo(
      {@required this.puntos,
      @required this.rayas,
      @required this.color,
      @required this.id,
      Key key})
      : super(key: key);

  final Color color;
  final int puntos;
  final int rayas;
  final int id;

  @override
  Widget build(BuildContext context) {
    String texto = '';
    String textoPuntos = '';
    String textoRayas = '';
    bool esCero = false;
    if (puntos == 0 && rayas == 0) {
      texto = '⌽';
      esCero = true;
    } else {
      if (puntos > 0) {
        for (int i = 0; i < puntos; i++) {
          textoPuntos = textoPuntos + '●';
          //○
        }
      }
      if (rayas > 0) {
        for (int i = 0; i < rayas; i++) {
          textoRayas = textoRayas + '⸻\n';
        }
      }
      texto = '$textoPuntos\n$textoRayas';
    }
    if (esCero) {
      return Container(
        height: 100,
        width: 100,
        margin: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/zero.png'),
          ),
        ),
      );
    } else {
      return Container(
        height: 100,
        width: 100,
        //color: color,
        margin: const EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            '$texto',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 25, height: 0.7),
          ),
        ),
      );
    }
  }
}
