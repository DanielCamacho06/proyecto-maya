import 'package:flutter/material.dart';

class Deslizador extends StatelessWidget {
  const Deslizador(
      {@required this.habilitado,
      @required this.valor,
      @required this.max,
      @required this.divisions,
      @required this.funcion,
      Key key})
      : super(key: key);

  final bool habilitado;
  final double valor;
  final double max;
  final int divisions;
  final void Function(double valor) funcion;

  @override
  Widget build(BuildContext context) {
    return Slider(
      value: valor,
      min: 0,
      max: max,
      divisions: divisions,
      activeColor: Colors.blue,
      label: valor.toString(),
      onChanged: (double value) {
        if (habilitado) {
          funcion(value);
        }
      },
    );
  }
}
