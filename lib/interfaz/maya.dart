import 'dart:math';

import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maya/aplicacion/bloc.dart';
import 'package:maya/aplicacion/estado.dart';
import 'package:flutter/material.dart';
import 'package:maya/dominio/operaciones.dart';
import 'package:maya/interfaz/sliders.dart';
import 'cubo.dart';

//true = Maya a Arábigo
//false = Arábigo a maya
bool bandera = true;
final controlador = TextEditingController();

class Maya extends StatelessWidget {
  const Maya({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BlocSlider(),
      child: MaterialApp(
        theme: ThemeData(
          brightness: Brightness.dark,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Números Mayas', textAlign: TextAlign.center),
          ),
          body: Center(
            child: SingleChildScrollView(
              child: BlocBuilder<BlocSlider, EstadoSlider>(
                  builder: (contextBB, estado) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //Título
                        Center(
                          child: Text(
                            estado.valorTextoTitulo,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                height: 1.5, fontSize: 25, color: Colors.pink),
                          ),
                        ),
                        if (bandera)
                          Center(
                            child: Text(
                              estado.valorTextoInstrucciones +
                                  estado.valorNumAleatorio.toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(height: 2, fontSize: 25),
                            ),
                          ),
                        if (!bandera)
                          Center(
                            child: Row(
                              children: [
                                Text(
                                  estado.valorTextoInstrucciones,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(height: 2, fontSize: 25),
                                ),
                                Container(
                                  height: 50,
                                  width: 100,
                                  child: TextField(
                                    controller: controlador,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Número',
                                    ),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        //Fin Título
                        Column(
                          children: [
                            //Nivel 3
                            Row(
                              children: [
                                crearCubos(
                                    numNivel: 3,
                                    valorPuntos: estado.valorCubo1,
                                    valorRayas: estado.valorCubo2),
                                Column(
                                  children: [
                                    Deslizador(
                                        habilitado: estado.habilitado,
                                        valor: estado.valorSlider1,
                                        max: 4,
                                        divisions: 4,
                                        funcion: contextBB
                                            .watch<BlocSlider>()
                                            .cambioSlider1),
                                    Deslizador(
                                        habilitado: estado.habilitado,
                                        valor: estado.valorSlider2,
                                        max: 3,
                                        divisions: 3,
                                        funcion: contextBB
                                            .watch<BlocSlider>()
                                            .cambioSlider2),
                                  ],
                                )
                              ],
                            ),
                            //Fin nivel 3
                            //Nivel 2
                            Row(
                              children: [
                                crearCubos(
                                    numNivel: 3,
                                    valorPuntos: estado.valorCubo3,
                                    valorRayas: estado.valorCubo4),
                                Column(
                                  children: [
                                    Deslizador(
                                        habilitado: estado.habilitado,
                                        valor: estado.valorSlider3,
                                        max: 4,
                                        divisions: 4,
                                        funcion: contextBB
                                            .watch<BlocSlider>()
                                            .cambioSlider3),
                                    Deslizador(
                                        habilitado: estado.habilitado,
                                        valor: estado.valorSlider4,
                                        max: 3,
                                        divisions: 3,
                                        funcion: contextBB
                                            .watch<BlocSlider>()
                                            .cambioSlider4),
                                  ],
                                ),
                              ],
                            ),
                            //Fin Nivel2
                            //Nivel 1
                            Row(
                              children: [
                                crearCubos(
                                    numNivel: 3,
                                    valorPuntos: estado.valorCubo5,
                                    valorRayas: estado.valorCubo6),
                                Column(
                                  children: [
                                    Deslizador(
                                        habilitado: estado.habilitado,
                                        valor: estado.valorSlider5,
                                        max: 4,
                                        divisions: 4,
                                        funcion: contextBB
                                            .watch<BlocSlider>()
                                            .cambioSlider5),
                                    Deslizador(
                                        habilitado: estado.habilitado,
                                        valor: estado.valorSlider6,
                                        max: 3,
                                        divisions: 3,
                                        funcion: contextBB
                                            .watch<BlocSlider>()
                                            .cambioSlider6),
                                  ],
                                ),
                              ],
                            ),
                            //Fin Nivel 1
                            Row(
                              children: [
                                FloatingActionButton(
                                  onPressed: () {
                                    cambiarConversion(contextBB);
                                  },
                                  child: Icon(Icons.compare_arrows),
                                  backgroundColor: Colors.orangeAccent,
                                ),
                                Container(
                                  height: 80,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        //Botón Recargar
                                        FloatingActionButton(
                                          child: Icon(Icons.refresh),
                                          backgroundColor: Colors.pink,
                                          onPressed: () {
                                            recargar(contextBB);
                                          },
                                        ),
                                        //Fin Botón Recargar
                                      ],
                                    ),
                                  ),
                                ),
                                //Botón Verificar
                                FloatingActionButton(
                                  onPressed: () {
                                    if (controlador.text.isEmpty &&
                                        bandera == false) {
                                      dialogoVacio(context);
                                    } else {
                                      bool correcto;
                                      contar(estado);
                                      correcto = verificar(estado, correcto);
                                      dialogo(context, correcto);
                                    }
                                  },
                                  child: Icon(Icons.check),
                                  backgroundColor: Colors.lightGreen,
                                ),
                                //Fin Botón verificar
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                );
              }),
            ),
          ),
        ),
        debugShowCheckedModeBanner: false,
      ),
    );
  }

  void dialogoVacio(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ), //this right here
            child: Container(
              height: 200,
              width: 500,
              child: Padding(
                padding: const EdgeInsets.all(50.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('Introduce un número.'),
                    SizedBox(
                      width: double.maxFinite,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Cerrar",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.orange,
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void cambiarConversion(BuildContext contextBB) {
    if (bandera) {
      contextBB.read<BlocSlider>().cambioConversion('Maya a Arábigo');
      contextBB.read<BlocSlider>().cambioTitulo('Maya a Arábigo');
      contextBB.read<BlocSlider>().cambiarSliderAleatorio();
      contextBB.read<BlocSlider>().cambioHabilitado();
      contextBB
          .read<BlocSlider>()
          .cambioInstruccion('Escribe el número Maya en Arábigo: ');
      bandera = false;
    } else {
      contextBB.read<BlocSlider>().cambioConversion('Arábigo a Maya');
      contextBB.read<BlocSlider>().cambioTitulo('Arábigo a Maya');
      contextBB.read<BlocSlider>().cambiarSliderInicializar();
      contextBB.read<BlocSlider>().cambioHabilitado();
      contextBB
          .read<BlocSlider>()
          .cambioInstruccion('Representa el siguiente número en Maya: ');
      bandera = true;
    }
  }

  void recargar(BuildContext contextBB) {
    if (bandera) {
      contextBB.read<BlocSlider>().cambiarNumAleatorio(Random().nextInt(400));
      contextBB.read<BlocSlider>().cambiarSliderInicializar();
    } else {
      contextBB.read<BlocSlider>().cambiarSliderAleatorio();
      controlador.clear();
    }
  }

  bool verificar(EstadoSlider estado, bool correcto) {
    if (bandera) {
      if (numeroMaya == estado.valorNumAleatorio) {
        correcto = true;
      } else {
        correcto = false;
      }
    } else {
      if (numeroMaya == int.parse(controlador.text)) {
        print('iguales');
        correcto = true;
      } else {
        print('noiguales');
        correcto = false;
      }
    }
    return correcto;
  }

  void dialogo(BuildContext context, bool correcto) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ), //this right here
            child: Container(
              height: 200,
              width: 500,
              child: Padding(
                padding: const EdgeInsets.all(50.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (correcto) Text('Correcto!'),
                    if (!correcto)
                      if (bandera)
                        Text('Incorrecto! Número seleccionado: $numeroMaya'),
                    if (!correcto)
                      if (!bandera)
                        Text('Incorrecto! el número no es equivalente al Maya'),
                    SizedBox(
                      width: double.maxFinite,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Cerrar",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.orange,
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  void contar(EstadoSlider estado) {
    numeroMaya = 0;
    numeroMaya = numeroMaya + contarPuntos(3, estado.valorSlider1.toInt());
    numeroMaya = numeroMaya + contarRayas(3, estado.valorSlider2.toInt());
    numeroMaya = numeroMaya + contarPuntos(2, estado.valorSlider3.toInt());
    numeroMaya = numeroMaya + contarRayas(2, estado.valorSlider4.toInt());
    numeroMaya = numeroMaya + contarPuntos(1, estado.valorSlider5.toInt());
    numeroMaya = numeroMaya + contarRayas(1, estado.valorSlider6.toInt());
  }
}

Widget crearCubos({int numNivel, int valorPuntos, int valorRayas}) {
  Widget cubo = Cubo(
    id: numNivel,
    color: Colors.blue,
    puntos: valorPuntos,
    rayas: valorRayas,
  );
  return cubo;
}
