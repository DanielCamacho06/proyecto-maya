import 'dart:math';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'estado_slider.freezed.dart';

@freezed
abstract class EstadoSlider with _$EstadoSlider {
  factory EstadoSlider({
    //1,2.. puntos y rayas respectivamente.
    @required double valorSlider1,
    @required double valorSlider2,
    @required double valorSlider3,
    @required double valorSlider4,
    @required double valorSlider5,
    @required double valorSlider6,
    @required int valorCubo1,
    @required int valorCubo2,
    @required int valorCubo3,
    @required int valorCubo4,
    @required int valorCubo5,
    @required int valorCubo6,
    @required String valorTextoTitulo,
    @required String valorTextoBoton,
    @required bool habilitado,
    @required String valorTextoInstrucciones,
    @required int valorNumAleatorio,
  }) = _EstadoSlider;
  factory EstadoSlider.inicial() => EstadoSlider(
        valorSlider1: 0,
        valorSlider2: 0,
        valorSlider3: 0,
        valorSlider4: 0,
        valorSlider5: 0,
        valorSlider6: 0,
        valorCubo1: 0,
        valorCubo3: 0,
        valorCubo2: 0,
        valorCubo4: 0,
        valorCubo5: 0,
        valorCubo6: 0,
        valorTextoTitulo: 'Arábigo a Maya',
        valorTextoBoton: 'Maya a Arábigo',
        habilitado: true,
        valorTextoInstrucciones: 'Representa el siguiente número en Maya: ',
        valorNumAleatorio: Random().nextInt(401),
      );
}
