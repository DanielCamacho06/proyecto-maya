// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'estado.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$EstadoSliderTearOff {
  const _$EstadoSliderTearOff();

// ignore: unused_element
  _EstadoSlider call(
      {@required double valorSlider1,
      @required double valorSlider2,
      @required double valorSlider3,
      @required double valorSlider4,
      @required double valorSlider5,
      @required double valorSlider6,
      @required int valorCubo1,
      @required int valorCubo2,
      @required int valorCubo3,
      @required int valorCubo4,
      @required int valorCubo5,
      @required int valorCubo6,
      @required String valorTextoTitulo,
      @required String valorTextoBoton,
      @required bool habilitado,
      @required String valorTextoInstrucciones,
      @required int valorNumAleatorio}) {
    return _EstadoSlider(
      valorSlider1: valorSlider1,
      valorSlider2: valorSlider2,
      valorSlider3: valorSlider3,
      valorSlider4: valorSlider4,
      valorSlider5: valorSlider5,
      valorSlider6: valorSlider6,
      valorCubo1: valorCubo1,
      valorCubo2: valorCubo2,
      valorCubo3: valorCubo3,
      valorCubo4: valorCubo4,
      valorCubo5: valorCubo5,
      valorCubo6: valorCubo6,
      valorTextoTitulo: valorTextoTitulo,
      valorTextoBoton: valorTextoBoton,
      habilitado: habilitado,
      valorTextoInstrucciones: valorTextoInstrucciones,
      valorNumAleatorio: valorNumAleatorio,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $EstadoSlider = _$EstadoSliderTearOff();

/// @nodoc
mixin _$EstadoSlider {
//1,2.. puntos y rayas respectivamente.
  double get valorSlider1;
  double get valorSlider2;
  double get valorSlider3;
  double get valorSlider4;
  double get valorSlider5;
  double get valorSlider6;
  int get valorCubo1;
  int get valorCubo2;
  int get valorCubo3;
  int get valorCubo4;
  int get valorCubo5;
  int get valorCubo6;
  String get valorTextoTitulo;
  String get valorTextoBoton;
  bool get habilitado;
  String get valorTextoInstrucciones;
  int get valorNumAleatorio;

  $EstadoSliderCopyWith<EstadoSlider> get copyWith;
}

/// @nodoc
abstract class $EstadoSliderCopyWith<$Res> {
  factory $EstadoSliderCopyWith(
          EstadoSlider value, $Res Function(EstadoSlider) then) =
      _$EstadoSliderCopyWithImpl<$Res>;
  $Res call(
      {double valorSlider1,
      double valorSlider2,
      double valorSlider3,
      double valorSlider4,
      double valorSlider5,
      double valorSlider6,
      int valorCubo1,
      int valorCubo2,
      int valorCubo3,
      int valorCubo4,
      int valorCubo5,
      int valorCubo6,
      String valorTextoTitulo,
      String valorTextoBoton,
      bool habilitado,
      String valorTextoInstrucciones,
      int valorNumAleatorio});
}

/// @nodoc
class _$EstadoSliderCopyWithImpl<$Res> implements $EstadoSliderCopyWith<$Res> {
  _$EstadoSliderCopyWithImpl(this._value, this._then);

  final EstadoSlider _value;
  // ignore: unused_field
  final $Res Function(EstadoSlider) _then;

  @override
  $Res call({
    Object valorSlider1 = freezed,
    Object valorSlider2 = freezed,
    Object valorSlider3 = freezed,
    Object valorSlider4 = freezed,
    Object valorSlider5 = freezed,
    Object valorSlider6 = freezed,
    Object valorCubo1 = freezed,
    Object valorCubo2 = freezed,
    Object valorCubo3 = freezed,
    Object valorCubo4 = freezed,
    Object valorCubo5 = freezed,
    Object valorCubo6 = freezed,
    Object valorTextoTitulo = freezed,
    Object valorTextoBoton = freezed,
    Object habilitado = freezed,
    Object valorTextoInstrucciones = freezed,
    Object valorNumAleatorio = freezed,
  }) {
    return _then(_value.copyWith(
      valorSlider1: valorSlider1 == freezed
          ? _value.valorSlider1
          : valorSlider1 as double,
      valorSlider2: valorSlider2 == freezed
          ? _value.valorSlider2
          : valorSlider2 as double,
      valorSlider3: valorSlider3 == freezed
          ? _value.valorSlider3
          : valorSlider3 as double,
      valorSlider4: valorSlider4 == freezed
          ? _value.valorSlider4
          : valorSlider4 as double,
      valorSlider5: valorSlider5 == freezed
          ? _value.valorSlider5
          : valorSlider5 as double,
      valorSlider6: valorSlider6 == freezed
          ? _value.valorSlider6
          : valorSlider6 as double,
      valorCubo1: valorCubo1 == freezed ? _value.valorCubo1 : valorCubo1 as int,
      valorCubo2: valorCubo2 == freezed ? _value.valorCubo2 : valorCubo2 as int,
      valorCubo3: valorCubo3 == freezed ? _value.valorCubo3 : valorCubo3 as int,
      valorCubo4: valorCubo4 == freezed ? _value.valorCubo4 : valorCubo4 as int,
      valorCubo5: valorCubo5 == freezed ? _value.valorCubo5 : valorCubo5 as int,
      valorCubo6: valorCubo6 == freezed ? _value.valorCubo6 : valorCubo6 as int,
      valorTextoTitulo: valorTextoTitulo == freezed
          ? _value.valorTextoTitulo
          : valorTextoTitulo as String,
      valorTextoBoton: valorTextoBoton == freezed
          ? _value.valorTextoBoton
          : valorTextoBoton as String,
      habilitado:
          habilitado == freezed ? _value.habilitado : habilitado as bool,
      valorTextoInstrucciones: valorTextoInstrucciones == freezed
          ? _value.valorTextoInstrucciones
          : valorTextoInstrucciones as String,
      valorNumAleatorio: valorNumAleatorio == freezed
          ? _value.valorNumAleatorio
          : valorNumAleatorio as int,
    ));
  }
}

/// @nodoc
abstract class _$EstadoSliderCopyWith<$Res>
    implements $EstadoSliderCopyWith<$Res> {
  factory _$EstadoSliderCopyWith(
          _EstadoSlider value, $Res Function(_EstadoSlider) then) =
      __$EstadoSliderCopyWithImpl<$Res>;
  @override
  $Res call(
      {double valorSlider1,
      double valorSlider2,
      double valorSlider3,
      double valorSlider4,
      double valorSlider5,
      double valorSlider6,
      int valorCubo1,
      int valorCubo2,
      int valorCubo3,
      int valorCubo4,
      int valorCubo5,
      int valorCubo6,
      String valorTextoTitulo,
      String valorTextoBoton,
      bool habilitado,
      String valorTextoInstrucciones,
      int valorNumAleatorio});
}

/// @nodoc
class __$EstadoSliderCopyWithImpl<$Res> extends _$EstadoSliderCopyWithImpl<$Res>
    implements _$EstadoSliderCopyWith<$Res> {
  __$EstadoSliderCopyWithImpl(
      _EstadoSlider _value, $Res Function(_EstadoSlider) _then)
      : super(_value, (v) => _then(v as _EstadoSlider));

  @override
  _EstadoSlider get _value => super._value as _EstadoSlider;

  @override
  $Res call({
    Object valorSlider1 = freezed,
    Object valorSlider2 = freezed,
    Object valorSlider3 = freezed,
    Object valorSlider4 = freezed,
    Object valorSlider5 = freezed,
    Object valorSlider6 = freezed,
    Object valorCubo1 = freezed,
    Object valorCubo2 = freezed,
    Object valorCubo3 = freezed,
    Object valorCubo4 = freezed,
    Object valorCubo5 = freezed,
    Object valorCubo6 = freezed,
    Object valorTextoTitulo = freezed,
    Object valorTextoBoton = freezed,
    Object habilitado = freezed,
    Object valorTextoInstrucciones = freezed,
    Object valorNumAleatorio = freezed,
  }) {
    return _then(_EstadoSlider(
      valorSlider1: valorSlider1 == freezed
          ? _value.valorSlider1
          : valorSlider1 as double,
      valorSlider2: valorSlider2 == freezed
          ? _value.valorSlider2
          : valorSlider2 as double,
      valorSlider3: valorSlider3 == freezed
          ? _value.valorSlider3
          : valorSlider3 as double,
      valorSlider4: valorSlider4 == freezed
          ? _value.valorSlider4
          : valorSlider4 as double,
      valorSlider5: valorSlider5 == freezed
          ? _value.valorSlider5
          : valorSlider5 as double,
      valorSlider6: valorSlider6 == freezed
          ? _value.valorSlider6
          : valorSlider6 as double,
      valorCubo1: valorCubo1 == freezed ? _value.valorCubo1 : valorCubo1 as int,
      valorCubo2: valorCubo2 == freezed ? _value.valorCubo2 : valorCubo2 as int,
      valorCubo3: valorCubo3 == freezed ? _value.valorCubo3 : valorCubo3 as int,
      valorCubo4: valorCubo4 == freezed ? _value.valorCubo4 : valorCubo4 as int,
      valorCubo5: valorCubo5 == freezed ? _value.valorCubo5 : valorCubo5 as int,
      valorCubo6: valorCubo6 == freezed ? _value.valorCubo6 : valorCubo6 as int,
      valorTextoTitulo: valorTextoTitulo == freezed
          ? _value.valorTextoTitulo
          : valorTextoTitulo as String,
      valorTextoBoton: valorTextoBoton == freezed
          ? _value.valorTextoBoton
          : valorTextoBoton as String,
      habilitado:
          habilitado == freezed ? _value.habilitado : habilitado as bool,
      valorTextoInstrucciones: valorTextoInstrucciones == freezed
          ? _value.valorTextoInstrucciones
          : valorTextoInstrucciones as String,
      valorNumAleatorio: valorNumAleatorio == freezed
          ? _value.valorNumAleatorio
          : valorNumAleatorio as int,
    ));
  }
}

/// @nodoc
class _$_EstadoSlider implements _EstadoSlider {
  _$_EstadoSlider(
      {@required this.valorSlider1,
      @required this.valorSlider2,
      @required this.valorSlider3,
      @required this.valorSlider4,
      @required this.valorSlider5,
      @required this.valorSlider6,
      @required this.valorCubo1,
      @required this.valorCubo2,
      @required this.valorCubo3,
      @required this.valorCubo4,
      @required this.valorCubo5,
      @required this.valorCubo6,
      @required this.valorTextoTitulo,
      @required this.valorTextoBoton,
      @required this.habilitado,
      @required this.valorTextoInstrucciones,
      @required this.valorNumAleatorio})
      : assert(valorSlider1 != null),
        assert(valorSlider2 != null),
        assert(valorSlider3 != null),
        assert(valorSlider4 != null),
        assert(valorSlider5 != null),
        assert(valorSlider6 != null),
        assert(valorCubo1 != null),
        assert(valorCubo2 != null),
        assert(valorCubo3 != null),
        assert(valorCubo4 != null),
        assert(valorCubo5 != null),
        assert(valorCubo6 != null),
        assert(valorTextoTitulo != null),
        assert(valorTextoBoton != null),
        assert(habilitado != null),
        assert(valorTextoInstrucciones != null),
        assert(valorNumAleatorio != null);

  @override //1,2.. puntos y rayas respectivamente.
  final double valorSlider1;
  @override
  final double valorSlider2;
  @override
  final double valorSlider3;
  @override
  final double valorSlider4;
  @override
  final double valorSlider5;
  @override
  final double valorSlider6;
  @override
  final int valorCubo1;
  @override
  final int valorCubo2;
  @override
  final int valorCubo3;
  @override
  final int valorCubo4;
  @override
  final int valorCubo5;
  @override
  final int valorCubo6;
  @override
  final String valorTextoTitulo;
  @override
  final String valorTextoBoton;
  @override
  final bool habilitado;
  @override
  final String valorTextoInstrucciones;
  @override
  final int valorNumAleatorio;

  @override
  String toString() {
    return 'EstadoSlider(valorSlider1: $valorSlider1, valorSlider2: $valorSlider2, valorSlider3: $valorSlider3, valorSlider4: $valorSlider4, valorSlider5: $valorSlider5, valorSlider6: $valorSlider6, valorCubo1: $valorCubo1, valorCubo2: $valorCubo2, valorCubo3: $valorCubo3, valorCubo4: $valorCubo4, valorCubo5: $valorCubo5, valorCubo6: $valorCubo6, valorTextoTitulo: $valorTextoTitulo, valorTextoBoton: $valorTextoBoton, habilitado: $habilitado, valorTextoInstrucciones: $valorTextoInstrucciones, valorNumAleatorio: $valorNumAleatorio)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EstadoSlider &&
            (identical(other.valorSlider1, valorSlider1) ||
                const DeepCollectionEquality()
                    .equals(other.valorSlider1, valorSlider1)) &&
            (identical(other.valorSlider2, valorSlider2) ||
                const DeepCollectionEquality()
                    .equals(other.valorSlider2, valorSlider2)) &&
            (identical(other.valorSlider3, valorSlider3) ||
                const DeepCollectionEquality()
                    .equals(other.valorSlider3, valorSlider3)) &&
            (identical(other.valorSlider4, valorSlider4) ||
                const DeepCollectionEquality()
                    .equals(other.valorSlider4, valorSlider4)) &&
            (identical(other.valorSlider5, valorSlider5) ||
                const DeepCollectionEquality()
                    .equals(other.valorSlider5, valorSlider5)) &&
            (identical(other.valorSlider6, valorSlider6) ||
                const DeepCollectionEquality()
                    .equals(other.valorSlider6, valorSlider6)) &&
            (identical(other.valorCubo1, valorCubo1) ||
                const DeepCollectionEquality()
                    .equals(other.valorCubo1, valorCubo1)) &&
            (identical(other.valorCubo2, valorCubo2) ||
                const DeepCollectionEquality()
                    .equals(other.valorCubo2, valorCubo2)) &&
            (identical(other.valorCubo3, valorCubo3) ||
                const DeepCollectionEquality()
                    .equals(other.valorCubo3, valorCubo3)) &&
            (identical(other.valorCubo4, valorCubo4) ||
                const DeepCollectionEquality()
                    .equals(other.valorCubo4, valorCubo4)) &&
            (identical(other.valorCubo5, valorCubo5) ||
                const DeepCollectionEquality()
                    .equals(other.valorCubo5, valorCubo5)) &&
            (identical(other.valorCubo6, valorCubo6) ||
                const DeepCollectionEquality()
                    .equals(other.valorCubo6, valorCubo6)) &&
            (identical(other.valorTextoTitulo, valorTextoTitulo) ||
                const DeepCollectionEquality()
                    .equals(other.valorTextoTitulo, valorTextoTitulo)) &&
            (identical(other.valorTextoBoton, valorTextoBoton) ||
                const DeepCollectionEquality()
                    .equals(other.valorTextoBoton, valorTextoBoton)) &&
            (identical(other.habilitado, habilitado) ||
                const DeepCollectionEquality()
                    .equals(other.habilitado, habilitado)) &&
            (identical(
                    other.valorTextoInstrucciones, valorTextoInstrucciones) ||
                const DeepCollectionEquality().equals(
                    other.valorTextoInstrucciones, valorTextoInstrucciones)) &&
            (identical(other.valorNumAleatorio, valorNumAleatorio) ||
                const DeepCollectionEquality()
                    .equals(other.valorNumAleatorio, valorNumAleatorio)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(valorSlider1) ^
      const DeepCollectionEquality().hash(valorSlider2) ^
      const DeepCollectionEquality().hash(valorSlider3) ^
      const DeepCollectionEquality().hash(valorSlider4) ^
      const DeepCollectionEquality().hash(valorSlider5) ^
      const DeepCollectionEquality().hash(valorSlider6) ^
      const DeepCollectionEquality().hash(valorCubo1) ^
      const DeepCollectionEquality().hash(valorCubo2) ^
      const DeepCollectionEquality().hash(valorCubo3) ^
      const DeepCollectionEquality().hash(valorCubo4) ^
      const DeepCollectionEquality().hash(valorCubo5) ^
      const DeepCollectionEquality().hash(valorCubo6) ^
      const DeepCollectionEquality().hash(valorTextoTitulo) ^
      const DeepCollectionEquality().hash(valorTextoBoton) ^
      const DeepCollectionEquality().hash(habilitado) ^
      const DeepCollectionEquality().hash(valorTextoInstrucciones) ^
      const DeepCollectionEquality().hash(valorNumAleatorio);

  @override
  _$EstadoSliderCopyWith<_EstadoSlider> get copyWith =>
      __$EstadoSliderCopyWithImpl<_EstadoSlider>(this, _$identity);
}

abstract class _EstadoSlider implements EstadoSlider {
  factory _EstadoSlider(
      {@required double valorSlider1,
      @required double valorSlider2,
      @required double valorSlider3,
      @required double valorSlider4,
      @required double valorSlider5,
      @required double valorSlider6,
      @required int valorCubo1,
      @required int valorCubo2,
      @required int valorCubo3,
      @required int valorCubo4,
      @required int valorCubo5,
      @required int valorCubo6,
      @required String valorTextoTitulo,
      @required String valorTextoBoton,
      @required bool habilitado,
      @required String valorTextoInstrucciones,
      @required int valorNumAleatorio}) = _$_EstadoSlider;

  @override //1,2.. puntos y rayas respectivamente.
  double get valorSlider1;
  @override
  double get valorSlider2;
  @override
  double get valorSlider3;
  @override
  double get valorSlider4;
  @override
  double get valorSlider5;
  @override
  double get valorSlider6;
  @override
  int get valorCubo1;
  @override
  int get valorCubo2;
  @override
  int get valorCubo3;
  @override
  int get valorCubo4;
  @override
  int get valorCubo5;
  @override
  int get valorCubo6;
  @override
  String get valorTextoTitulo;
  @override
  String get valorTextoBoton;
  @override
  bool get habilitado;
  @override
  String get valorTextoInstrucciones;
  @override
  int get valorNumAleatorio;
  @override
  _$EstadoSliderCopyWith<_EstadoSlider> get copyWith;
}
