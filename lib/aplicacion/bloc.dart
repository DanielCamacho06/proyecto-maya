import 'dart:math';

import 'package:bloc/bloc.dart';
import 'estado.dart';

class BlocSlider extends Cubit<EstadoSlider> {
  BlocSlider() : super(EstadoSlider.inicial());

  void cambioHabilitado() {
    emit(state.copyWith(habilitado: !state.habilitado));
  }

  void cambioSlider1(double cantidad) {
    emit(state.copyWith(valorSlider1: cantidad));
    emit(state.copyWith(valorCubo1: cantidad.toInt()));
  }

  void cambioSlider2(double cantidad) {
    emit(state.copyWith(valorSlider2: cantidad));
    emit(state.copyWith(valorCubo2: cantidad.toInt()));
  }

  void cambioSlider3(double cantidad) {
    emit(state.copyWith(valorSlider3: cantidad));
    emit(state.copyWith(valorCubo3: cantidad.toInt()));
  }

  void cambioSlider4(double cantidad) {
    emit(state.copyWith(valorSlider4: cantidad));
    emit(state.copyWith(valorCubo4: cantidad.toInt()));
  }

  void cambioSlider5(double cantidad) {
    emit(state.copyWith(valorSlider5: cantidad));
    emit(state.copyWith(valorCubo5: cantidad.toInt()));
  }

  void cambioSlider6(double cantidad) {
    emit(state.copyWith(valorSlider6: cantidad));
    emit(state.copyWith(valorCubo6: cantidad.toInt()));
  }

  void cambioConversion(String texto) {
    emit(state.copyWith(valorTextoBoton: texto));
  }

  void cambioTitulo(String texto) {
    emit(state.copyWith(valorTextoTitulo: texto));
  }

  void cambioInstruccion(String texto) {
    emit(state.copyWith(valorTextoInstrucciones: texto));
  }

  void cambiarSliderAleatorio() {
    double numeroAleatorio = Random().nextInt(2).toDouble();
    emit(state.copyWith(valorSlider1: numeroAleatorio));
    emit(state.copyWith(valorCubo1: numeroAleatorio.toInt()));
    //numeroAleatorio = Random().nextInt(3).toDouble();
    //emit(state.copyWith(valorSlider2: numeroAleatorio));
    //emit(state.copyWith(valorCubo2: numeroAleatorio.toInt()));
    emit(state.copyWith(valorSlider2: 0));
    emit(state.copyWith(valorCubo2: 0));
    numeroAleatorio = Random().nextInt(4).toDouble();
    emit(state.copyWith(valorSlider3: numeroAleatorio));
    emit(state.copyWith(valorCubo3: numeroAleatorio.toInt()));
    numeroAleatorio = Random().nextInt(3).toDouble();
    emit(state.copyWith(valorSlider4: numeroAleatorio));
    emit(state.copyWith(valorCubo4: numeroAleatorio.toInt()));
    numeroAleatorio = Random().nextInt(4).toDouble();
    emit(state.copyWith(valorSlider5: numeroAleatorio));
    emit(state.copyWith(valorCubo5: numeroAleatorio.toInt()));
    numeroAleatorio = Random().nextInt(3).toDouble();
    emit(state.copyWith(valorSlider6: numeroAleatorio));
    emit(state.copyWith(valorCubo6: numeroAleatorio.toInt()));
  }

  void cambiarSliderInicializar() {
    emit(state.copyWith(valorSlider1: 0));
    emit(state.copyWith(valorCubo1: 0));
    emit(state.copyWith(valorSlider2: 0));
    emit(state.copyWith(valorCubo2: 0));
    emit(state.copyWith(valorSlider3: 0));
    emit(state.copyWith(valorCubo3: 0));
    emit(state.copyWith(valorSlider4: 0));
    emit(state.copyWith(valorCubo4: 0));
    emit(state.copyWith(valorSlider5: 0));
    emit(state.copyWith(valorCubo5: 0));
    emit(state.copyWith(valorSlider6: 0));
    emit(state.copyWith(valorCubo6: 0.toInt()));
  }

  void cambiarNumAleatorio(int numero) {
    emit(state.copyWith(valorNumAleatorio: numero));
  }
}
