int numeroMaya = 0;

int contarPuntos(int nivel, int cantidad) {
  int suma = 0;
  if (cantidad != 0) {
    if (nivel == 1) {
      suma = cantidad;
    }
    if (nivel == 2) {
      suma = (cantidad * 20);
    }
    if (nivel == 3) {
      suma = (cantidad * (20 * 20));
    }
  }
  return suma;
}

int contarRayas(int nivel, int cantidad) {
  int suma = 0;
  if (cantidad != 0) {
    if (nivel == 1) {
      suma = (cantidad * 5);
    }
    if (nivel == 2) {
      suma = (cantidad * 20) * 5;
    }
    if (nivel == 3) {
      suma = (cantidad * (20 * 20) * 5);
    }
  }
  return suma;
}
